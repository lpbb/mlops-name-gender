import pandas as pd
from fastapi import FastAPI
import numpy as np
import joblib

model = joblib.load( 'model.joblib')
alphabet = "abcdefghijklmnopqrstuvwxyz-"

def encode_prenom(x):
    output = [letter in x.upper() for letter in alphabet.upper()]
    return pd.Series(output)

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/prediction/{item}")
def read_item(item: str):
    encoded_name = np.array(encode_prenom(item).astype(int))
    result = model.predict(encoded_name.reshape(1, -1))
    sexe = ""
    if result[0] == 0:
        sexe = "F"
    else:
        sexe = "M"
    return sexe









